//
//  APIKey.h
//  SarMel
//
//  Created by tunlin on 4/12/15.
//  Copyright (c) 2015 Bixels. All rights reserved.
//

#ifndef SarMel_APIKey_h
#define SarMel_APIKey_h

static NSString *const kGoggleMapAPIKey     = @"google_map_key";
static NSString *const kParseApplicationID  = @"parse_application_id";
static NSString *const kParseClientKey      = @"parse_client_key";

#endif
