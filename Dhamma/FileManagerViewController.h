//
//  FileManagerViewController.h
//  Dhamma
//
//  Created by tunlin on 9/13/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrawerViewController.h"
#import "MZDownloadManagerViewController.h"
#import "FBFilesTableViewController.h"

@interface FileManagerViewController : FBFilesTableViewController

@end
