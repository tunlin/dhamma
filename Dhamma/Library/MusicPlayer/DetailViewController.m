//
//  DetailViewController.m
//  BackgroundAudioObjc
//
//  Created by Jonathan Sagorin on 3/4/2015.
//
//

#import "DetailViewController.h"
#import "TestMusicPlayer.h"

@interface DetailViewController ()
@property (strong, nonatomic) TestMusicPlayer *musicPlayer;

@property (weak, nonatomic) IBOutlet UILabel *songTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *playPauseButton;
@end

@implementation DetailViewController
@synthesize filePath;

#pragma mark - Managing the detail item
- (void)configureView {
    NSString *fileName = [self.filePath lastPathComponent];
    self.songTitleLabel.text = fileName;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    [TestMusicPlayer initSession];
    self.musicPlayer = [[TestMusicPlayer alloc]init];

}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: self.filePath];

    [self.musicPlayer playSongWithURL:url];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
    
    [self.musicPlayer clear];
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - user actions
-(IBAction)playPauseButtonTapped:(UIButton*)button
{
    if ([button.titleLabel.text isEqualToString:@"Pause"]) {
        [self.musicPlayer pause];
        [button setTitle:@"Play" forState:UIControlStateNormal];
    } else {
        [self.musicPlayer play];
        [button setTitle:@"Pause" forState:UIControlStateNormal];
    }
}

#pragma mark - remote control events
- (void) remoteControlReceivedWithEvent: (UIEvent *) receivedEvent {
    [self.musicPlayer remoteControlReceivedWithEvent:receivedEvent];
}

#pragma mark - audio session management
- (BOOL) canBecomeFirstResponder {
    return YES;
}

@end



