//
//  UtilityManager.h
//  Dhamma
//
//  Created by Win Tun on 6/9/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilityManager : NSObject {
    NSString *info;
}

@property (nonatomic, retain) NSString *info;


+ (id)sharedManager;



@end
