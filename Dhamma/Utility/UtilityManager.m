//
//  UtilityManager.m
//  Dhamma
//
//  Created by Win Tun on 6/9/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import "UtilityManager.h"

#import <AFNetworking.h>

@implementation UtilityManager

@synthesize info;

+ (id)sharedManager {
    static UtilityManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        info = @"default value";
        [self monitorNetwork];
        [self monitorServer];
    }
    
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

/**
 *  monitor network change
 */
- (void) monitorNetwork {
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));

    }];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
}

/**
 * monitor backend server
 */
- (void) monitorServer {
    NSURL *baseURL = [NSURL URLWithString:@"http://www.dhammadownload.com/"];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    
    NSOperationQueue *operationQueue = manager.operationQueue;
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                [operationQueue setSuspended:NO];
                // TODO network Okay
                break;
            case AFNetworkReachabilityStatusNotReachable:
            default:
                [operationQueue setSuspended:YES];
                // TODO not reachable
                break;
        }
    }];
    
    [manager.reachabilityManager startMonitoring];
}

@end
