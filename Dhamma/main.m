//
//  main.m
//  Dhamma
//
//  Created by tunlin on 5/10/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
