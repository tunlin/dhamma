#import <UIKit/UIKit.h>
#import "DrawerViewController.h"

@interface CenterViewController : DrawerViewController
@property (weak, nonatomic) IBOutlet UIView *containerView;

- (void) switchController:(NSInteger ) pageId;
@end
