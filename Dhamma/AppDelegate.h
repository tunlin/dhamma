//
//  AppDelegate.h
//  Dhamma
//
//  Created by tunlin on 5/10/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (copy) void (^backgroundSessionCompletionHandler)();

@end

