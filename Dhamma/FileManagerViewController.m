//
//  FileManagerViewController.m
//  Dhamma
//
//  Created by tunlin on 9/13/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import "FileManagerViewController.h"
#import "DrawerNavigationController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMExampleDrawerVisualStateManager.h"

#import "MMLogoView.h"
#import "MMDrawerBarButtonItem.h"
#import "MMCenterTableViewCell.h"

#import "LeftSideDrawerViewController.h"
#import "RightSideDrawerViewController.h"

#import <QuartzCore/QuartzCore.h>


@interface FileManagerViewController ()

@end

@implementation FileManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Setup menu for drawer
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    

}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    UINavigationController *parentViewController = (UINavigationController *)self.parentViewController;
    NSLog(@"%@", [parentViewController.childViewControllers description]);
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}


@end
