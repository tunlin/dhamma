// Copyright (c) 2013 Mutual Mobile (http://mutualmobile.com/)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#import "LeftSideDrawerViewController.h"
#import "MMTableViewCell.h"
#import "CenterViewController.h"

#import "DhammaViewController.h"
#import "FileManagerViewController.h"
#import "FileAvailableViewController.h"
#import "FileDownloadingViewController.h"
#import "FBFilesTableViewController.h"

@interface LeftSideDrawerViewController () {
    DhammaViewController *dhammaViewController;
    FileManagerViewController *fileManagerViewController;
    
    
    FileAvailableViewController *fileAvailableViewController;
    FileDownloadingViewController *filedownloadingViewController;
}

@end

@implementation LeftSideDrawerViewController

-(id)init{
    self = [super init];
    if(self){
        [self setRestorationIdentifier:@"MMExampleLeftSideDrawerController"];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Developer" bundle:nil];
        dhammaViewController        = [[DhammaViewController alloc] init];
        
        // fileManagerViewController = [storyboard instantiateViewControllerWithIdentifier:@"filemanager_viewcontroller"];
        // fileManagerViewController = [fileManagerViewController initWithPath:@"/"];
        // fileManagerViewController = [fileManagerViewController initWithPath:fileDest];

        fileManagerViewController = [[FileManagerViewController alloc] initWithPath:fileDest];
        
        fileAvailableViewController = [storyboard instantiateViewControllerWithIdentifier:@"download_viewcontroller"];
        
        filedownloadingViewController = [storyboard instantiateViewControllerWithIdentifier:@"downloading_viewcontroller"];
        
        fileAvailableViewController.downloadingViewController = filedownloadingViewController;
    }
    
    return  self;
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self setTitle:@"Main Menu"];
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [super tableView:tableView titleForHeaderInSection:section];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    

    NSLog(@"parent : %@", self.parentViewController);
    NSLog(@"parent : %@", self.mm_drawerController.centerViewController);
    UIViewController *newViewController = NULL;
    
    switch (indexPath.section) {
            
        case MDDrawerSectionDhammadownloadMain: {
            
            if (indexPath.row == 0) {
                newViewController = dhammaViewController;
                
            } else if (indexPath.row == 1) {
                newViewController = fileManagerViewController;
            }
            break;
        }
        case MDDrawerSectionDhammadownloadProgress : {
            if (indexPath.row == 0) {
                newViewController = fileAvailableViewController;
            } else if (indexPath.row == 1) {
                newViewController = filedownloadingViewController;
            }
            break;
        }
        default:
            break;
    }
    
    // Reset navigation controller in center
    UINavigationController *drawerNavigationController =
    (UINavigationController *)self.mm_drawerController.centerViewController;
    
    [drawerNavigationController setToolbarHidden:YES];
    [drawerNavigationController setViewControllers:nil];
    [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    
    
    // Update title
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *cellText = selectedCell.textLabel.text;
    [newViewController setTitle:cellText];
    
    // Switch view controller
    [drawerNavigationController setViewControllers:[NSArray arrayWithObject:newViewController] animated:YES];

}

@end
