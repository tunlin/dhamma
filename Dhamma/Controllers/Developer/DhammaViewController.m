//
//  DhammaViewController.m
//  Dhamma
//
//  Created by tunlin on 9/12/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import "DhammaViewController.h"

#import "DrawerNavigationController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMExampleDrawerVisualStateManager.h"

#import "MMLogoView.h"
#import "MMDrawerBarButtonItem.h"
#import "MMCenterTableViewCell.h"

#import "LeftSideDrawerViewController.h"
#import "RightSideDrawerViewController.h"



#import "LinkManager.h"

#import <QuartzCore/QuartzCore.h>

typedef NS_ENUM(NSInteger, MMCenterViewControllerSection){
    MMCenterViewControllerSectionLeftViewState,
    MMCenterViewControllerSectionLeftDrawerAnimation,
    MMCenterViewControllerSectionRightViewState,
    MMCenterViewControllerSectionRightDrawerAnimation,
};

@interface DhammaViewController ()

@end

@implementation DhammaViewController

- (id)init
{
    self = [super init];
    if (self) {
        [self setRestorationIdentifier:@"MMExampleCenterControllerRestorationKey"];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    LinkManager *linkManager = [LinkManager sharedManager];
    [self setDelegate:linkManager];
    
    [self setTitle:@"Dhamma Download"];
    
    // URL loading
    super.showsURLInNavigationBar       = YES;
    self.showsURLInNavigationBar        = NO;
    self.showsPageTitleInNavigationBar  = NO;
    [super loadURLString:@"http://www.dhammadownload.com/"];
    
    // gesture
    UITapGestureRecognizer * doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    [doubleTap setNumberOfTapsRequired:2];
    [self.view addGestureRecognizer:doubleTap];
    
    UITapGestureRecognizer * twoFingerDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(twoFingerDoubleTap:)];
    [twoFingerDoubleTap setNumberOfTapsRequired:2];
    [twoFingerDoubleTap setNumberOfTouchesRequired:2];
    [self.view addGestureRecognizer:twoFingerDoubleTap];
    
    [self setupLeftMenuButton];
    
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    UINavigationController *parentViewController = (UINavigationController *)self.parentViewController;
    NSLog(@"%@", [parentViewController.childViewControllers description]);
    
}


-(void)setupLeftMenuButton{
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
}

#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

-(void)rightDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideRight animated:YES completion:nil];
}

-(void)doubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideLeft completion:nil];
}

-(void)twoFingerDoubleTap:(UITapGestureRecognizer*)gesture{
    [self.mm_drawerController bouncePreviewForDrawerSide:MMDrawerSideRight completion:nil];
}







@end
