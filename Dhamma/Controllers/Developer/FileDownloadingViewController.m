//
//  FileDownloadingViewController.m
//  Dhamma
//
//  Created by tunlin on 9/13/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import "FileDownloadingViewController.h"
#import "DrawerNavigationController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMExampleDrawerVisualStateManager.h"

#import "MMLogoView.h"
#import "MMDrawerBarButtonItem.h"
#import "MMCenterTableViewCell.h"

#import "LeftSideDrawerViewController.h"
#import "RightSideDrawerViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "MZDownloadManagerViewController.h"

@interface FileDownloadingViewController ()

@end

@implementation FileDownloadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UITapGestureRecognizer * doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    [doubleTap setNumberOfTapsRequired:2];
    [self.view addGestureRecognizer:doubleTap];
    
    UITapGestureRecognizer * twoFingerDoubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(twoFingerDoubleTap:)];
    [twoFingerDoubleTap setNumberOfTapsRequired:2];
    [twoFingerDoubleTap setNumberOfTouchesRequired:2];
    [self.view addGestureRecognizer:twoFingerDoubleTap];
    
    
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    
}

#pragma mark - Button Handlers
-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)downloadRequestStarted:(NSURLSessionDownloadTask *)downloadTask {
    NSLog(@"%s", __func__);
    // [self updateDownloadingTabBadge];
}

- (void)downloadRequestCanceled:(NSURLSessionDownloadTask *)downloadTask {
    NSLog(@"%s", __func__);
    // [self updateDownloadingTabBadge];
}

- (void)downloadRequestFinished:(NSString *)fileName {
    NSLog(@"%s", __func__);
    
    // [self updateDownloadingTabBadge];
    NSString *docDirectoryPath = [fileDest stringByAppendingPathComponent:fileName];
    [[NSNotificationCenter defaultCenter] postNotificationName:DownloadCompletedNotif object:docDirectoryPath];
    NSLog(@"Dir : %@", docDirectoryPath);
    NSLog(@"Filename : %@", fileName);
}

@end
