//
//  FileAvailableViewController.m
//  Dhamma
//
//  Created by tunlin on 9/13/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import "FileAvailableViewController.h"

#import "DrawerNavigationController.h"
#import "UIViewController+MMDrawerController.h"
#import "MMExampleDrawerVisualStateManager.h"

#import "MMLogoView.h"
#import "MMDrawerBarButtonItem.h"
#import "MMCenterTableViewCell.h"

#import "LeftSideDrawerViewController.h"
#import "RightSideDrawerViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "MZDownloadManagerViewController.h"
#import "FileDownloadingViewController.h"

#import "LinkManager.h"

@interface FileAvailableViewController () <MZDownloadDelegate> {

}

@end

@implementation FileAvailableViewController

@synthesize availableDownloadsArray = _availableDownloadsArray;
@synthesize availableDownloadTableView = _availableDownloadTableView;
@synthesize downloadingViewController = _downloadingViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    
    // self.availableDownloadsArray = [[NSMutableArray alloc] init];
    
    /*
    self.availableDownloadsArray = [NSMutableArray arrayWithObjects:
                               @"http://dl.dropbox.com/u/97700329/file1.mp4",
                               @"http://dl.dropbox.com/u/97700329/file2.mp4",
                               @"http://dl.dropbox.com/u/97700329/file3.mp4",
                               @"http://dl.dropbox.com/u/97700329/FileZilla_3.6.0.2_i686-apple-darwin9.app.tar.bz2",
                               @"http://dl.dropbox.com/u/97700329/GCDExample-master.zip", nil];
    */
    self.availableDownloadsArray = [[NSMutableArray alloc] init];
    [_downloadingViewController setDelegate:self];
    _downloadingViewController.downloadingArray = [[NSMutableArray alloc] init];
    _downloadingViewController.sessionManager = [_downloadingViewController backgroundSession];
    [_downloadingViewController populateOtherDownloadTasks];
}

#pragma mark
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    

    LinkManager *linkManager = [LinkManager sharedManager];
    NSMutableArray *newArray =
    [[NSMutableArray alloc] initWithArray:linkManager.availableDownloadsArray
                                copyItems:YES];
    [self.availableDownloadsArray removeAllObjects];    
    [self.availableDownloadsArray addObjectsFromArray:newArray];
    
    NSLog(@"%s", __func__);
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"%s", __func__);
    
    [self.availableDownloadTableView reloadData];
}

-(void)leftDrawerButtonPress:(id)sender{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma mark - Tableview Delegate and Datasource -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.availableDownloadsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"AvailableDownloadsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell.textLabel setText:[[[self.availableDownloadsArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"/"] lastObject]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *urlLastPathComponent = [[[self.availableDownloadsArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"/"] lastObject];
    NSString *fileName = [MZUtility getUniqueFileNameForName:urlLastPathComponent];
        [_downloadingViewController addDownloadTask:fileName
                                            fileURL:[self.availableDownloadsArray objectAtIndex:
                                                     indexPath.row]];
    // TODO updating tab badge
    [self.availableDownloadsArray removeObjectAtIndex:indexPath.row];
    [self.availableDownloadTableView deleteRowsAtIndexPaths:@[indexPath]
                                           withRowAnimation:UITableViewRowAnimationAutomatic];
}


#pragma mark - MZDownloadDelegate
- (void)downloadRequestStarted:(NSURLSessionDownloadTask *)downloadTask {
    NSLog(@"%s", __func__);
    // [self updateDownloadingTabBadge];
}

- (void)downloadRequestCanceled:(NSURLSessionDownloadTask *)downloadTask {
    NSLog(@"%s", __func__);
    // [self updateDownloadingTabBadge];
}

- (void)downloadRequestFinished:(NSString *)fileName {
    NSLog(@"%s", __func__);
    
    // [self updateDownloadingTabBadge];
    NSString *docDirectoryPath = [fileDest stringByAppendingPathComponent:fileName];
    [[NSNotificationCenter defaultCenter] postNotificationName:DownloadCompletedNotif object:docDirectoryPath];
    NSLog(@"Dir : %@", docDirectoryPath);
    NSLog(@"Filename : %@", fileName);
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
}



@end
