//
//  LinkManager.m
//  Dhamma
//
//  Created by tunlin on 9/13/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import "LinkManager.h"
#import <Bolts.h>
#import "HTMLNode.h"
#import "HTMLParser.h"

@implementation LinkManager {
    NSString *currentPage;
}

@synthesize currentLink;
@synthesize availableDownloadsArray;

+ (id)sharedManager {
    static LinkManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        currentLink = @"Default Property Value";
        availableDownloadsArray = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark - KINWebBrowserDelegate Protocol Implementation
- (void)webBrowser:(KINWebBrowserViewController *)webBrowser didStartLoadingURL:(NSURL *)URL {
    NSLog(@"Started Loading URL : %@", URL);
    
    return;
    
    // TODO check url final path
    NSString *pathextension = [URL pathExtension];
    
    NSArray *fileType = @[@"mp3",
                          @"mp4",
                          // @"pdf",
                          @"wma"];
    
    if ([fileType containsObject:pathextension]) {
        NSLog(@"Need to filter :%@", pathextension);
        [webBrowser.wkWebView stopLoading];
        
        self.currentLink = [URL absoluteString];
        
        NSString *message = [NSString stringWithFormat:@"URL : %@", [URL lastPathComponent]];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:@"Link Detection"
                                                           message:message
                                                          delegate:self
                                                 cancelButtonTitle:@"Cancel"
                                                 otherButtonTitles:@"Download", nil];
        [alerView show];
        
        [[BFTask taskWithResult:nil] continueWithExecutor:[BFExecutor defaultExecutor] withBlock:^id(BFTask *task) {
         
            if (webBrowser.wkWebView) {
                NSError *error;
                HTMLParser *parser = [[HTMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:currentPage] error:&error];
                HTMLNode *bodyNode = [parser body];
                
                HTMLNode *linkNode = [bodyNode findChildWithAttribute:@"href" matchingName:[URL absoluteString] allowPartial:FALSE];
                HTMLNode *node = [linkNode children][0];
                
                NSString *value = nil;
                if ([node nodetype] == HTMLTextNode) {
                    value = [linkNode contents];
                } else {
                    value = [node contents];
                }
                NSString *trimmedString = [value stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSLog(@"Content : %@", trimmedString);
            }
            return nil;
        }];
    } else {
        NSLog(@"Okay to go : %@", pathextension);
    }
}

- (void)webBrowser:(KINWebBrowserViewController *)webBrowser didFinishLoadingURL:(NSURL *)URL {
    NSLog(@"Finished Loading URL : %@", URL);
    currentPage = [URL absoluteString];
}

- (void)webBrowser:(KINWebBrowserViewController *)webBrowser didFailToLoadURL:(NSURL *)URL withError:(NSError *)error {
    NSLog(@"Failed To Load URL : %@ With Error: %@", URL, error);
}

- (void)webBrowserViewControllerWillDismiss:(KINWebBrowserViewController*)viewController {
    NSLog(@"View Controller will dismiss: %@", viewController);
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"Button Index :%ld", buttonIndex);
    switch (buttonIndex) {
        case 0:
            break;
        case 1: {
            [self.availableDownloadsArray addObject:[NSString stringWithString:currentLink]];
            break;
        }
        default:
            break;
    }
    
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            break;
        }
        case 1: {
            
            break;
        }
        default:
            break;
    }
    NSLog(@"Button Index :%ld", buttonIndex);
}


@end
