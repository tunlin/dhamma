//
//  LinkManager.h
//  Dhamma
//
//  Created by tunlin on 9/13/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <KINWebBrowser/KINWebBrowserViewController.h>

@interface LinkManager : NSObject <KINWebBrowserDelegate, UIAlertViewDelegate> {
    NSString *currentLink;
}

@property (nonatomic, retain) NSString *currentLink;
@property (atomic, strong) NSMutableArray *availableDownloadsArray;

+ (id)sharedManager;

@end
