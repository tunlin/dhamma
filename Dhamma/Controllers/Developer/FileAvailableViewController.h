//
//  FileAvailableViewController.h
//  Dhamma
//
//  Created by tunlin on 9/13/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FileDownloadingViewController.h"

@interface FileAvailableViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *availableDownloadTableView;

@property (atomic, strong) NSMutableArray *availableDownloadsArray;
@property (atomic, weak) FileDownloadingViewController *downloadingViewController;

@end
