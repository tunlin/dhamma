//
//  SecondViewController.h
//  Dhamma
//
//  Created by tunlin on 5/10/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AvailableViewController : UIViewController


@property (atomic, strong) NSMutableArray *availableDownloadsArray;


@end

