//
//  SecondViewController.m
//  Dhamma
//
//  Created by tunlin on 5/10/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import "AvailableViewController.h"
#import "MZDownloadManagerViewController.h"

@interface AvailableViewController() <MZDownloadDelegate> {
 
    IBOutlet UITableView *availableDownloadTableView;
    MZDownloadManagerViewController *mzDownloadingViewObj;
}

@end


@implementation AvailableViewController

@synthesize availableDownloadsArray = _availableDownloadsArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    /*
    availableDownloadsArray = [NSMutableArray arrayWithObjects:
                               @"http://dl.dropbox.com/u/97700329/file1.mp4",
                               @"http://dl.dropbox.com/u/97700329/file2.mp4",
                               @"http://dl.dropbox.com/u/97700329/file3.mp4",
                               @"http://dl.dropbox.com/u/97700329/FileZilla_3.6.0.2_i686-apple-darwin9.app.tar.bz2",
                               @"http://dl.dropbox.com/u/97700329/GCDExample-master.zip", nil];
    */
    self.availableDownloadsArray = [[NSMutableArray alloc] init];
    
    
    UINavigationController *mzDownloadingNav = [self.tabBarController.viewControllers objectAtIndex:2];
    mzDownloadingViewObj = [mzDownloadingNav.viewControllers objectAtIndex:0];
    [mzDownloadingViewObj setDelegate:self];
    
    mzDownloadingViewObj.downloadingArray = [[NSMutableArray alloc] init];
    mzDownloadingViewObj.sessionManager = [mzDownloadingViewObj backgroundSession];
    [mzDownloadingViewObj populateOtherDownloadTasks];
    
    [self updateDownloadingTabBadge];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"%s", __func__);
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"%s", __func__);
    
    [availableDownloadTableView reloadData];
    
}

#pragma mark - My Methods -
- (void)updateDownloadingTabBadge
{
    UITabBarItem *downloadingTab = [self.tabBarController.tabBar.items objectAtIndex:2];
    int badgeCount = mzDownloadingViewObj.downloadingArray.count;
    if(badgeCount == 0)
        [downloadingTab setBadgeValue:nil];
    else
        [downloadingTab setBadgeValue:[NSString stringWithFormat:@"%d",badgeCount]];
}
#pragma mark - Tableview Delegate and Datasource -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.availableDownloadsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AvailableDownloadsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    [cell.textLabel setText:[[[self.availableDownloadsArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"/"] lastObject]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *urlLastPathComponent = [[[self.availableDownloadsArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"/"] lastObject];
    NSString *fileName = [MZUtility getUniqueFileNameForName:urlLastPathComponent];
    [mzDownloadingViewObj addDownloadTask:fileName fileURL:[self.availableDownloadsArray objectAtIndex:indexPath.row]];
    
    [self updateDownloadingTabBadge];
    
    [self.availableDownloadsArray removeObjectAtIndex:indexPath.row];
    [availableDownloadTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}
#pragma mark - MZDownloadManager Delegates -
-(void)downloadRequestStarted:(NSURLSessionDownloadTask *)downloadTask
{
    [self updateDownloadingTabBadge];
}
-(void)downloadRequestFinished:(NSString *)fileName
{
    [self updateDownloadingTabBadge];
    NSString *docDirectoryPath = [fileDest stringByAppendingPathComponent:fileName];
    [[NSNotificationCenter defaultCenter] postNotificationName:DownloadCompletedNotif object:docDirectoryPath];
}
-(void)downloadRequestCanceled:(NSURLSessionDownloadTask *)downloadTask
{
    [self updateDownloadingTabBadge];
}

# pragma mark - download

- (IBAction) onShowClicked:(id)sender {
    NSLog(@"available : %@", [_availableDownloadsArray description]);
}
@end
