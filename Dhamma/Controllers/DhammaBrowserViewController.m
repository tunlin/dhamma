//
//  FirstViewController.m
//  Dhamma
//
//  Created by tunlin on 5/10/15.
//  Copyright (c) 2015 Bixel. All rights reserved.
//

#import "DhammaBrowserViewController.h"
#import "WebViewController.h"
#import "AvailableViewController.h"

#import "Reachability.h"


@interface DhammaBrowserViewController () {
    NSURL *currentDownloadUrl;
}

@property (nonatomic) Reachability *hostReachability;
@property (nonatomic) Reachability *internetReachability;
@property (nonatomic) Reachability *wifiReachability;

@end

static NSString *const defaultAddress = @"http://www.apple.com/";
static NSString *const dhammaAddress = @"http://www.dhammadownload.com/";


@implementation DhammaBrowserViewController
@synthesize webrowserContainer = _webrowserContainer;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    if ([self.childViewControllers count] == 0) {
        WebViewController *webBrowser = [WebViewController webBrowser];
        [webBrowser setDelegate:self];
        [_webrowserContainer addSubview:webBrowser.view];
        [self addChildViewController:webBrowser];
        [webBrowser loadURLString:dhammaAddress];
    }
    
    
    

    // Check network reachability
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    NSString *remoteHostName = dhammaAddress;

    self.hostReachability = [Reachability reachabilityWithHostName:remoteHostName];
    [self.hostReachability startNotifier];
    [self updateInterfaceWithReachability:self.hostReachability];
}

/**
 *  <#Description#>
 *
 *  @param reachability <#reachability description#>
 */
- (void)updateInterfaceWithReachability:(Reachability *)reachability {

    NetworkStatus status = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];

    NSLog(@"status :%ld, connection required : %d",
          (long)status,
          connectionRequired);
}

/**
 *  <#Description#>
 *
 *  @param note <#note description#>
 */
- (void) reachabilityChanged:(NSNotification *)note  {
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}



- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
}

#pragma mark - KINWebBrowserDelegate Protocol Implementation

- (void)webBrowser:(WebViewController *)webBrowser didStartLoadingURL:(NSURL *)URL {
    NSLog(@"Started Loading URL : %@", URL);
}

- (void)webBrowser:(WebViewController *)webBrowser didFinishLoadingURL:(NSURL *)URL {
    NSLog(@"Finished Loading URL : %@", URL);
}

- (void)webBrowser:(WebViewController *)webBrowser didFailToLoadURL:(NSURL *)URL withError:(NSError *)error {
    NSLog(@"Failed To Load URL : %@ With Error: %@", URL, error);
}

- (void)webBrowser:(WebViewController *)webBrowser detectedMediaLoadingURL:(NSURL *)URL {
    NSLog(@"Detected Media : %@", URL);
    
    currentDownloadUrl = URL;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Download", nil];

    [actionSheet showInView:self.view];
    
}


/**
 *  <#Description#>
 *
 *  @param actionSheet <#actionSheet description#>
 *  @param buttonIndex <#buttonIndex description#>
 */
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0: {
            UINavigationController *mzDownloadingNav = [self.tabBarController.viewControllers objectAtIndex:1];
            AvailableViewController *downloadViewController= [mzDownloadingNav.viewControllers objectAtIndex:0];
            [downloadViewController.availableDownloadsArray addObject:[currentDownloadUrl absoluteString]];
            
            break;
        }
        default:
            break;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"%s segue:%@", __func__, segue.identifier);
    
    //    if ([segue.identifier isEqualToString:@"containerLogin"])
    //        self.vcLogin = (LoginVC *)segue.destinationViewController;
    //
    //    if ([segue.identifier isEqualToString:@"containerStartNew"])
    //        self.vcStartNew = (StartNewVC *)segue.destinationViewController;
    
}



/*
 // Launch webrowser in nav mode
 
 WebViewController *webBrowser = [WebViewController webBrowser];
 [webBrowser setDelegate:self];
 [self.navigationController pushViewController:webBrowser animated:YES];
 [webBrowser loadURLString:dhammaAddress];

 */


/*
 
 UINavigationController *webBrowserNavigationController = [WebViewController navigationControllerWithWebBrowser];
 WebViewController *webBrowser = [webBrowserNavigationController rootWebBrowser];
 [webBrowser setDelegate:self];
 webBrowser.showsURLInNavigationBar = YES;
 webBrowser.tintColor = [UIColor whiteColor];
 webBrowser.barTintColor = [UIColor colorWithRed:102.0f/255.0f green:204.0f/255.0f blue:51.0f/255.0f alpha:1.0f];
 webBrowser.showsPageTitleInNavigationBar = NO;
 webBrowser.showsURLInNavigationBar = NO;
 [self presentViewController:webBrowserNavigationController animated:YES completion:nil];
 
 [webBrowser loadURLString:dhammaAddress];
 
 
 */





@end
